
from pathlib import Path
from typing import Union
import random

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from fastai.data import *
from fastai.data.all import *

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

# adversarial attack imports
from art.estimators.classification import KerasClassifier, TensorFlowV2Classifier
from art.attacks.evasion import FastGradientMethod, CarliniLInfMethod

import tcav

PLOTS_DIR = Path("plots") / "adversarial"
PLOTS_DIR.mkdir(parents=True, exist_ok=True)

path = untar_data(URLs.IMAGENETTE_320)

# tf.compat.v1.disable_eager_execution()

fnames = get_image_files(path)

# these are from fastai tutorial info.  is there not an easy way to get the id -> label?
label_dict = dict(
    n01440764="tench",
    n02102040="English springer",
    n02979186="cassette player",
    n03000684="chain saw",
    n03028079="church",
    n03394916="French horn",
    n03417042="garbage truck",
    n03425413="gas pump",
    n03445777="golf ball",
    n03888257="parachute",
)

label_array = list(label_dict.values())

BATCH_SIZE = 32
IMG_SIZE = (160, 160)
IMG_SHAPE = IMG_SIZE + (3,)


train_folder = path / "train"


def get_all_subfolder_images(folder):
    images = []
    labels = []
    for idx, key in enumerate(label_dict.keys()):
        label_folder = folder / key
        image_paths = list(label_folder.glob("*.JPEG"))
        imgs = [tf.keras.preprocessing.image.load_img(p, target_size=IMG_SIZE) for p in image_paths]
        imgs = [tf.keras.preprocessing.image.img_to_array(img) / 255 for img in imgs]
        labels_ = [idx for _ in range(len(imgs))]
        images.extend(imgs)
        labels.extend(labels_)

    return np.array(images), np.array(labels)


images, labels = get_all_subfolder_images(path / "train")
val_images, val_labels = get_all_subfolder_images(path / "val")

labels_binary = tf.keras.utils.to_categorical(labels)
val_labels_binary = tf.keras.utils.to_categorical(val_labels)

assert len(images) == len(labels)

# --------------

base_model = tf.keras.applications.vgg16.VGG16(
    include_top=False,
    input_shape=IMG_SHAPE,
    weights="imagenet"
)

for layer in base_model.layers:
    layer.trainable = False
# add new classifier layers
flat = tf.keras.layers.Flatten()(base_model.layers[-1].output)
# class1 = tf.keras.layers.Dense(1024, activation='relu')(flat1)
output = tf.keras.layers.Dense(labels_binary.shape[1], activation='softmax')(flat)
model = tf.keras.Model(inputs=base_model.inputs, outputs=output)

# summarize
model.summary()


base_learning_rate = 0.0001

loss_func = tf.keras.losses.CategoricalCrossentropy()
model.compile(optimizer=tf.keras.optimizers.Adam(lr=base_learning_rate),
              loss=loss_func,
              metrics=['accuracy'])

initial_epochs = 1
callbacks = [
        tf.keras.callbacks.EarlyStopping(patience=3),
    ]
history = model.fit(x=images,
                    y=labels_binary,
                    epochs=initial_epochs,
                    batch_size=32,
                    callbacks=callbacks,
                    validation_data=(val_images, val_labels_binary))

# classifier = KerasClassifier(model=model, clip_values=(0, 1))
classifier = TensorFlowV2Classifier(model=model, nb_classes=labels_binary.shape[1], input_shape=images.shape[1:],
                                    clip_values=(0,1), loss_object=loss_func)
attack_fgsm = FastGradientMethod(estimator=classifier, eps=0.03)


# use_n = 100

concepts = val_images
concept_labels = val_labels_binary

x_concepts, x_concepts_subset, y_concepts, y_concepts_subset = train_test_split(concepts, concept_labels, test_size=0.2)
# split again so we
x_concepts, x_concepts_subset, y_concepts, y_concepts_subset = train_test_split(x_concepts_subset, y_concepts_subset, shuffle=False, test_size=0.3)
print(f"len of x_concepts: {len(x_concepts)} len of x_concepts_subset: {len(x_concepts_subset)}")
print(f"first 4 classes should be shuffled?: {[np.argmax(val) for val in y_concepts[:4]]}")

# generate the variety of other things
# concepts_attack = attack_fgsm.generate(x_concepts)
# concepts_noise = np.random.uniform(size=x_concepts_subset.shape)


# create predictions for later on
# pred_concepts = model.predict(x_concepts_subset)
# pred_concepts_attack = model.predict(concepts_attack)
# pred_noise = model.predict(concepts_noise)

# heres what the adversarial attack looks like in general, these are fish images
# random_idx = random.randint(0, x_concepts_subset.shape[0])

# fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 10))


# ax1.matshow(x_concepts_subset[random_idx])
# ax2.matshow(x_concepts_subset[random_idx])

# ax1.set_title(f"predicted class for image: {label_array[pred_concepts[random_idx].argmax()]}")
# ax2.set_title(f"predicted class for attack: {label_array[pred_concepts_attack[random_idx].argmax()]}")


# fig.savefig("plots/adversarial/plot1.png")



# find the best
layer_n = 2
tcav = tcav.TCAV(model)

x_concepts_attack = attack_fgsm.generate(x_concepts)
x_concepts_subset_attack = attack_fgsm.generate(x_concepts_subset)
y_true = np.repeat([1, 0], [len(x_concepts_subset), len(x_concepts_subset_attack)])

best = {
    "layer": None,
    "score": 0
}


for layer_n in range(0, len(model.layers)-1):
    tcav.use_bottleneck(layer_n)
    # calculate sensitivity score relative to adversarial examples
    tcav.train_cav(x_concepts, x_concepts_attack)
    tcav.calculate_sensitivty(x_concepts_subset, y_concepts_subset, x_concepts_subset_attack, y_concepts_subset)
    scores = tcav.sensitivity_score()


    y_preds = tcav.test_lm(np.concatenate([x_concepts_subset, x_concepts_subset_attack]))



    score = accuracy_score(y_preds, y_true)
    print(f"for layer_n[{layer_n}] got accuracy score: {score}")

    if score > best["score"]:
        best["layer"] = layer_n
        best["score"] = score
        print(f"updated best score")


# using CAV for detecting at evaluation

tcav.use_bottleneck(best["layer"])
x_concepts_attack = attack_fgsm.generate(x_concepts)
x_concepts_subset_attack = attack_fgsm.generate(x_concepts_subset)



# use best
# tcav.train_cav(x_concde)

# compare VGG to
vgg = tf.keras.applications.vgg16.VGG16()

for layer in vgg.layers:
    layer.trainable = False

vgg.add(tf.keras.layers.Dense(2, activation='softmax'))

