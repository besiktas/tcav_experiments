from dataclasses import dataclass

import numpy as np
import tensorflow as tf
import umap
from sklearn import cluster


ImagenetLabelDict = dict(
    n01440764="tench",
    n02102040="English springer",
    n02979186="cassette player",
    n03000684="chain saw",
    n03028079="church",
    n03394916="French horn",
    n03417042="garbage truck",
    n03425413="gas pump",
    n03445777="golf ball",
    n03888257="parachute",
)


@dataclass
class Dataset:
    _data = None
    _target = None

    @property
    def data(self):
        return self._data

    @property
    def target(self):
        return self._target


@dataclass
class DataHelper:
    _train: Dataset = None
    _test: Dataset = None

    # label_dict: dict = None
    # layer_idx: int = -1

    # _subset = None
    # _y_subset = None

    # def use_layer(self, idx: int):
    #     self.layer_idx = idx

    # def _load_data(self, data_dir="data/"):
    #     pass

    # def _transform_target(self):
    #     """
    #     if the target is one-hot it might need this
    #     """
    #     self.target = self.target.argmax(axis=1)

    # def data(self, idx=None):
    #     if idx:
    #         return self._data[idx].reshape(len(self._data[idx]), -1)
    #     else:
    #         return self._data

    # @property
    # def subset(self):
    #     return self._subset


def get_layer_output(model, idx, x):
    OutFunc = tf.keras.backend.function([model.input], [model.layers[idx].output])
    return OutFunc(np.expand_dims(x, axis=0))[0]


def each_layer_outputs(model, x):
    outputs = []
    for idx in range(1, len(model.layers)):
        outputs.append(get_layer_output(model, idx, x))
    return outputs


def generate_layer_outputs(data, model):
    layer_outputs = {}

    for idx, x in enumerate(data):
        x_outputs = each_layer_outputs(model, x)

        # for each layer activations
        for layer_idx, layer_activations in enumerate(x_outputs):

            if layer_idx not in layer_outputs:
                layer_outputs[layer_idx] = np.array(layer_activations)
            else:
                layer_outputs[layer_idx] = np.vstack((layer_outputs[layer_idx], layer_activations))

    return layer_outputs


def generate_layer_clusters(layer_outputs):

    layer_embedding_clusters = []

    for layer_n in range(0, max(layer_outputs.keys())):

        data = layer_outputs[layer_n].reshape(len(layer_outputs[idx]), -1)

        umap_obj = umap.UMAP(random_state=42)
        standard_embedding = umap_obj.fit_transform(data)

        clustering_obj = cluster.KMeans(n_clusters=10)
        cluster_labels = clustering_obj.fit_predict(standard_embedding)

        # append tuple of (layer, umap_obj, cluster_obj)
        layer_embedding_clusters.append((layer_n, umap_obj, clustering_obj))

    return layer_embedding_clusters


def avg_dist_best_cluster_each_class_per_layer(layer_clusters_array, layer_activations, labels, num_classes):
    average_distance_each_class_per_layer = {val: [] for val in range(0, num_classes)}
    best_cluster_each_class_per_layer = {val: [] for val in range(0, num_classes)}

    for layer_n, umap_obj, clustering_obj in layer_clusters_array:
        # for class 0 get the cluster
        for class_label in range(0, num_classes):
            x = layer_activations[layer_n][np.where(labels.argmax(axis=1) == class_label)[0]]
            x = x.reshape(len(x), -1)
            x_embedded = umap_obj.transform(x)

            # use this instead of predict to get distances
            x_clusters_distance = clustering_obj.transform(x_embedded)

            binned_ = np.histogram(x_clusters_distance.argmin(axis=1), bins=range(0, 10))
            # zip it together and then sort based on highest bin
            binned = sorted(list(zip(*binned_)), key=lambda x: x[0], reverse=True)

            best_cluster_each_class_per_layer[class_label].append(binned)
            best_cluster = best_cluster_each_class_per_layer[class_label][-1][0][1]

            average_distance = np.average([val[best_cluster] for val in x_clusters_distance])
            average_distance_each_class_per_layer[class_label].append(average_distance)

    return average_distance_each_class_per_layer