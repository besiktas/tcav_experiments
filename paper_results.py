import umap
import umap.plot
import numpy as np
import tensorflow as tf
from sklearn import cluster

from pathlib import Path

# import utils


def get_layer_output(model, idx, x):
    OutFunc = tf.keras.backend.function([model.input], [model.layers[idx].output])
    return OutFunc(np.expand_dims(x, axis=0))[0]


def each_layer_outputs(model, x):
    outputs = []
    for idx in range(1, len(model.layers)):
        outputs.append(get_layer_output(model, idx, x))
    return outputs


def generate_layer_outputs(data):
    layer_outputs = {}

    for idx, x in enumerate(data):
        x_outputs = each_layer_outputs(model, x)

        # for each layer activations
        for layer_idx, layer_activations in enumerate(x_outputs):

            if layer_idx not in layer_outputs:
                layer_outputs[layer_idx] = np.array(layer_activations)
            else:
                layer_outputs[layer_idx] = np.vstack((layer_outputs[layer_idx], layer_activations))

    return layer_outputs


x_concepts = np.load("data/x_concepts.npy")
x_concepts_subset = np.load("data/x_concepts_subset.npy")
y_concepts = np.load("data/y_concepts.npy")
y_concepts_subset = np.load("data/y_concepts_subset.npy")
x_concepts_attack = np.load("data/x_concepts_attack.npy")
x_concepts_subset_attack = np.load("data/x_concepts_subset_attack.npy")

num_classes = y_concepts.shape[1]

PLOTS_DIR = Path("plots") / "adversarial"
PLOTS_DIR.mkdir(parents=True, exist_ok=True)


# you will get an OOM error if you dont just take a bit of this.  need to refactor anything with predict otherwise
USE_SMALLER = 250
x_concepts = x_concepts[:USE_SMALLER]
x_concepts_subset = x_concepts_subset[:USE_SMALLER]
y_concepts = y_concepts[:USE_SMALLER]
y_concepts_subset = y_concepts_subset[:USE_SMALLER]
x_concepts_attack = x_concepts_attack[:USE_SMALLER]
x_concepts_subset_attack = x_concepts_subset_attack[:USE_SMALLER]

model = tf.keras.models.load_model("models/latest")

x_concepts_preds = model.predict(x_concepts)

layer_outputs = generate_layer_outputs(x_concepts)
layer_outputs_subset = generate_layer_outputs(x_concepts_subset)


layer_embedding_clusters = []
for layer_n in range(0, max(layer_outputs.keys())):

    data = layer_outputs[layer_n].reshape(len(layer_outputs[layer_n]), -1)
    target = y_concepts.argmax(axis=1)

    umap_obj = umap.UMAP(random_state=42)
    standard_embedding = umap_obj.fit_transform(data)

    clustering_obj = cluster.KMeans(n_clusters=10)
    cluster_labels = clustering_obj.fit_predict(standard_embedding)

    # append tuple of (layer, umap_obj, cluster_obj)
    layer_embedding_clusters.append((layer_n, umap_obj, clustering_obj))


# ===
#
# ===
average_distance_each_class_per_layer = {val: [] for val in range(0, num_classes)}
best_cluster_each_class_per_layer = {val: [] for val in range(0, num_classes)}


for layer_n, umap_obj, clustering_obj in layer_embedding_clusters:
    # for class 0 get the cluster
    for class_label in range(0, num_classes):
        x = layer_outputs[layer_n][np.where(y_concepts.argmax(axis=1) == class_label)[0]]
        x = x.reshape(len(x), -1)
        x_embedded = umap_obj.transform(x)

        # use this instead of predict to get distances
        x_clusters_distance = clustering_obj.transform(x_embedded)

        binned_ = np.histogram(x_clusters_distance.argmin(axis=1), bins=range(0, 10))
        # zip it together and then sort based on highest bin
        binned = sorted(list(zip(*binned_)), key=lambda x: x[0], reverse=True)

        best_cluster_each_class_per_layer[class_label].append(binned)
        best_cluster = best_cluster_each_class_per_layer[class_label][-1][0][1]

        average_distance = np.average([val[best_cluster] for val in x_clusters_distance])
        average_distance_each_class_per_layer[class_label].append(average_distance)