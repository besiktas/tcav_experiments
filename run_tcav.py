import argparse
import json
from pathlib import Path
import sys
from typing import List

import pandas as pd
import numpy as np
import tensorflow as tf

from cli import args
from constants import TARGETCOL, TIMESERIES_LENGTH
from log import get_logger
from train_nn import train_model
from tcav import TCAV
from utils import get_attack_info_df, create_concept

logger = get_logger("root")


def run_all_models():
    models_dir = (Path() / "models").resolve()

    models = [d for d in models_dir.iterdir() if d.is_dir()]
    print(models)


def main(attack_nums: List[int] = [10, 11], model_type: str = "base", model_df_dir: Path = Path(__file__).parent):
    run = wandb.init(project="tcav-experiments-swat", job_type="tcav_eval", tags=["tcav", args.model_type])

    model_path = model_df_dir / f"models/{model_type}"
    df_path = model_df_dir / "data/attack_cleaned.csv"

    # data needs to already be scaled/cleaned
    if not model_path.is_dir() or not df_path.is_file():
        raise Exception("model is not dir or df is not file")

    model = tf.keras.models.load_model(model_path)
    df = pd.read_csv(df_path)
    df = df.set_index(pd.DatetimeIndex(df["timestamp"]))
    df.drop(["timestamp"], inplace=True, axis=1)

    attack_info_df = get_attack_info_df(pdf_path=model_df_dir / "docs/List_of_attacks_Final.pdf")

    concept, counterexamples = create_concept(df, attack_info_df, [10, 11])

    concepts_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(
        concept.drop(TARGETCOL, axis=1).values,
        concept[TARGETCOL].values,
        length=TIMESERIES_LENGTH,
        batch_size=1,
        shuffle=True,
    )

    # use stride to balance the number of samples somehow?
    counterexamples_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(
        counterexamples.drop(TARGETCOL, axis=1).values,
        counterexamples[TARGETCOL].values,
        length=TIMESERIES_LENGTH,
        batch_size=1,
        stride=round(len(counterexamples) / len(concept)),
        shuffle=True,
    )

    # turning into arrays since easier to understand

    concepts_x = []
    concepts_y = []
    for x, y in concepts_gen:
        concepts_x.append(x)
        concepts_y.append(y)

    counterexamples_x = []
    counterexamples_y = []
    for x, y in counterexamples_gen:
        counterexamples_x.append(x)
        counterexamples_y.append(y)

    concepts_x = np.array(concepts_x).squeeze()
    concepts_y = np.array(concepts_y).squeeze()
    counterexamples_x = np.array(counterexamples_x).squeeze()
    counterexamples_y = np.array(counterexamples_y).squeeze()

    tcav = TCAV(model)

    model_json_filepath = f"models/json/model-json-{model_type}.json"
    with open(model_json_filepath, "w") as outfile:
        json.dump(model.to_json(), outfile)

    model_png_filepath = f"models/other/{model_type}.png"
    tf.keras.utils.plot_model(
        model,
        to_file=model_png_filepath,
        # show_shapes=False,
        # show_dtype=False,
        show_layer_names=True,
        rankdir="TB",
        expand_nested=False,
        dpi=96,
    )
    wandb.log({"keras_model": wandb.Image(model_png_filepath)})
    wandb.save(model_json_filepath)

    for layer_n in range(1, len(tcav.model.layers) - 1):

        tcav.use_bottleneck(layer_n)
        tcav.train_cav(concepts_x, counterexamples_x)

        tcav.calculate_sensitivty(concepts_x, counterexamples_x)
        sensitivity_score = tcav.sensitivity_score()

        log_obj = {
            "model_type": model_type,
            "sensitivity_score": sensitivity_score,
            "sensitivity": tcav.sensitivity,
            "grad_vals": tcav.grad_vals,
            "cav": tcav.cav,
            "layer_n": layer_n,
            "layer_type": tcav.bottleneck_layer.name,
        }

        logger.info(f"=== === ===")
        logger.info(f"sensitivity scores for LAYER: {layer_n} of type: {tcav.bottleneck_layer.name}")
        logger.info(f"[class 0 to concept] ==> {sensitivity_score[0]}")
        logger.info(f"[class 1 to concept] ==> {sensitivity_score[1]}")
        logger.info(f"=== === ===")

        wandb.log(log_obj)
        # used this as a sanity check to see if what i did was right, uncomment to manually check again
        # tcav.calculate_sensitivty_old(concepts_x[0], concepts_y[0])
        # print(f"OLD-tcav.sensitivity-{layer_n}: {tcav.sensitivity_old}")


if __name__ == "__main__":

    main(model_type=args.model_type)
    # run_all_models()