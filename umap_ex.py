import numpy as np
import tensorflow as tf
from sklearn.metrics import accuracy_score

import tcav as TCAV

x_concepts = np.load("data/x_concepts")
x_concepts_subset = np.load("data/x_concepts_subset")
y_concepts = np.load("data/y_concepts")
y_concepts_subset = np.load("data/y_concepts_subset")
x_concepts_attack = np.load("data/x_concepts_attack")
x_concepts_subset_attack = np.load("data/x_concepts_subset_attack")


model = tf.keras.models.load_model("models/latest")

tcav = TCAV.TCAV(model)

use_layer = 1

tcav.use_bottleneck(use_layer)
tcav.train_cav(x_concepts, x_concepts_attack)
tcav.calculate_sensitivty(x_concepts_subset, y_concepts_subset, x_concepts_subset_attack, y_concepts_subset)

y_preds = tcav.test_lm(np.concatenate([x_concepts_subset, x_concepts_subset_attack]))
y_true_subset = np.repeat([0, 1], [len(x_concepts_subset), len(x_concepts_subset_attack)])

score = accuracy_score(y_preds, y_true_subset)

print(f"using lm from tcav stuff accuracy={score}")


def make_detector_nn():
    