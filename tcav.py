import logging
from pathlib import Path
from typing import Union

import numpy as np
import tensorflow as tf
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score

logger = logging.getLogger(__name__)



def split_model(model, bottleneck: int):
    """
    split a model based on a bottleneck layer

    seperated from class because
    """

    if bottleneck < 0 or bottleneck >= len(model.layers):
        raise ValueError("Bottleneck layer must be greater than or equal to 0 and less than the number of layers!")

    model_f = tf.keras.Model(inputs=model.input, outputs=model.layers[bottleneck].output)

    # create model h functional
    model_h_input = tf.keras.layers.Input(model.layers[bottleneck + 1].input_shape[1:])
    model_h = model_h_input
    for layer in model.layers[bottleneck + 1 :]:
        model_h = layer(model_h)
    model_h = tf.keras.Model(inputs=model_h_input, outputs=model_h)

    return model_f, model_h

#
class TCAV(object):
    """tcav
    init with model
    then use_bottleneck
    then train_cav
    then calculate_sensitivty
    """

    def __init__(self, model, lm=None, bottleneck=None):
        self.model = model
        self.lm = SGDClassifier() if not lm else lm

        if bottleneck:
            self.use_bottleneck(bottleneck)
        else:
            self.bottleneck_layer = None
            self.model_h = None
            self.model_f = None

        # scores
        self.sensitivity = None
        self.labels = None

        # you want to use this on model.predict()  otherwise easy to get oom
        self.batch_size = 32

    def use_bottleneck(self, bottleneck: int):
        """split the model into 2 models based on model and post models for tcav linear model

        Args:
            layer (int): layer to split nn model
        """
        self.model_f, self.model_h = split_model(self.model, bottleneck=bottleneck)
        self.bottleneck_layer = self.model.layers[bottleneck]


    def train_cav(self, concepts, counterexamples):
        # get activations for both
        x = self.model_f.predict(np.concatenate([concepts, counterexamples]), batch_size=self.batch_size)
        x = x.reshape(x.shape[0], -1)

        y = np.repeat([0, 1], [len(concepts), len(counterexamples)])
        logger.info("fitting the linear model")
        # accr
        self.lm.fit(x, y)
        self.coefs = self.lm.coef_
        self.cav = np.transpose(-1 * self.coefs)

    def test_lm(self, x):
        activations = self.model_f.predict(x)
        return self.lm.predict(activations.reshape(len(activations), -1))

    def calculate_sensitivity(self, concepts, concepts_labels, counterexamples, counterexamples_labels):

        activations = np.concatenate(
            [
                self.model_f.predict(concepts, batch_size=self.batch_size),
                self.model_f.predict(counterexamples, batch_size=self.batch_size),
            ]
        )
        labels = np.concatenate([concepts_labels, counterexamples_labels])

        grad_vals = []

        for x, y in zip(activations, labels):
            # predicting on single thing so need single to be 1 batch
            x = tf.convert_to_tensor(np.expand_dims(x, axis=0), dtype=tf.float32)
            y = tf.convert_to_tensor(np.expand_dims(y, axis=0), dtype=tf.float32)

            with tf.GradientTape() as tape:
                tape.watch(x)

                y_out = self.model_h(x)
                loss = tf.keras.backend.categorical_crossentropy(y, y_out)

            grad_vals.append(tape.gradient(loss, x).numpy())

        grad_vals = np.array(grad_vals).squeeze()

        self.sensitivity = np.dot(grad_vals.reshape(grad_vals.shape[0], -1), self.cav)
        self.labels = labels
        self.grad_vals = grad_vals

    def sensitivity_score(self):
        """Print the sensitivities in a readable way"""
        num_classes = self.labels.shape[-1]

        sens_for_class_k = {}
        for k in range(0, num_classes):
            class_idxs = np.where(self.labels[:, k] == 1)
            if len(class_idxs[0]) == 0:
                sens_for_class_k[k] = None
            else:
                sens_for_class = self.sensitivity[class_idxs[0]]
                sens_for_class_k[k] = len(sens_for_class[sens_for_class > 0]) / len(sens_for_class)

        return sens_for_class_k

    def find_best_tcav(self, x_concepts, x_concepts_attack, x_concepts_subset, y_concepts_subset, x_concepts_subset_attack):
        """
        i need to probably rename these.  its unclear what everything is
        """
        self.best = {
            "layer": None,
            "score": 0,
        }

        score_per_layer = []

        y_true = np.repeat([0, 1], [len(x_concepts), len(x_concepts_attack)])
        y_true_subset = np.repeat([0, 1], [len(x_concepts_subset), len(x_concepts_subset_attack)])

        for layer_n in range(0, len(self.model.layers) - 1):

            self.use_bottleneck(layer_n)
            self.train_cav(x_concepts, x_concepts_attack)
            self.calculate_sensitivty(x_concepts_subset, y_concepts_subset, x_concepts_subset_attack, y_concepts_subset)

            y_preds = self.test_lm(np.concatenate([x_concepts_subset, x_concepts_subset_attack]))
            score = accuracy_score(y_preds, y_true_subset)

            if score > self.best["score"]:
                logger.info(f"best tcav layer updated using layer: {layer_n}")
                self.best = {"layer": layer_n, "score": score}

        return self.best





class TCAV_PYTORCH(object):

    """
    docstring
    """
    _called = 0

    def __pre_init(self):
        from torch import nn
        _called += 1

    def __init__(self, *args, **kwargs) -> None:
        if _called <= 0:
            self.__pre_init()

        super().__init__(*args, **kwargs)



    def use_bottleneck(self, bottleneck: int):
        self.model_f = nn.Sequential(*list(self.model.children())[:bottleneck])
        self.model_f = nn.Sequential(*list(self.model.children())[bottleneck:])
        self.bottleneck_layer = bottleneck


def get_all_missclassification_for_label(preds, idx):
    return np.argwhere(preds.argmax(axis=1) == idx)


def create_adversarial_pattern(input_image, input_label, model, loss_object, targeted=1):
    # https://github.com/soumyac1999/FGSM-Keras/blob/master/targeted_attack.ipynb
    # https://github.com/soumyac1999/FGSM-Keras/blob/master/untargeted_attack.ipynb
    # targeted = -1 if targeted else 1
    with tf.GradientTape() as tape:
        tape.watch(input_image)
        pred = model(input_image)
        loss = targeted * loss_object(input_label, pred)
    grad = tape.gradient(loss, input_image)
    signed_grad = tf.sign(grad)
    return signed_grad